var mongoose = require('mongoose');
var Bicicleta = require('../../public/models/bicicleta');
var request = require('request');
const { json } = require('express');
//var server = require('../../bin/www');





describe('Bicicleta API', () => {


    beforeAll(function (done) {


        var mongoDB = 'mongodb://localhost:27017/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connecction error'));
        db.once('open', function () {
            console.log('we are connected to test database');
            done();

        });

    });

    afterEach(function (done) {

        Bicicleta.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            done();

        });

    });



    //beforeEach(()=>{console.log('testeando...');});

    describe('Get Bicicleta /', () => {
        it('Status 200', (done) => {


            //expect(Bicicleta.allBicis.length).toBe(0);

            //var a = new Bicicleta(10, 'rojo', 'urbana', [10.988781, -74.7915428]);
            //Bicicleta.add(a);

            request.get('http://localhost:5000/api/bicicletas', function (error, response, body) {
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();

            });



        });
    });


    describe('POST BiCiCLETA /create', () => {
        it('Status 200', (done) => {


            var headers = { 'Content-Type': 'application/json' };
            const abici = { code: 12, color: "rojo", modelo: "urbana", lat: -34, lng: -51 };
            request.post({
                headers: headers,
                url: 'http://localhost:5000/api/bicicletas/create',
                body: JSON.stringify(abici),




            }, function (error, response, body) {


                if (error) return console.log(error);

                console.log(response.statusCode);
                var bici = JSON.parse(body);





                expect(response.statusCode).toBe(200);


                expect(abici.color).toEqual("rojo");
                expect(abici.lat).toBe(-34);
                expect(abici.lng).toBe(-51);
                Bicicleta.find({code:abici.code},function(err,bici){
                        if(err) console.log(err);
                        expect(bici.code).toBe(12);

                });
                //expect(Bicicleta.findById(10).color).toBe("rojo");
                done();

            });





        });
    });


});


var mongoose = require('mongoose');
var Bicicleta = require('../../public/models/bicicleta');
const bicicletaSchema = require('../../public/models/bicicleta');







describe("Testing Bicicletas", () => {

    beforeAll(function (done) {


        var mongoDB = 'mongodb://localhost:27017/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connecction error'));
        db.once('open', function () {
            console.log('we are connected to test database');
            done();

        });

    });

    afterEach(function (done) {

        Bicicleta.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            done();

        });

    });



    describe("Bicicleta.createInstance", () => {
        it('crea una instancia de Bicicleta', () => {


            var bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1]);



            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(-34.5);
            expect(bici.ubicacion[1]).toEqual(-54.1);
        })

    });

    describe("Bicicleta.allBicis", () => {

        it('comienza vacia', (done) => {
            Bicicleta.allBicis(function (err, bicis) {

                expect(bicis.length).toBe(0);
                done();

            });


        });
    });

    describe("Bicicleta.add", () => {

        it("agrega solo una bici", (done) => {
            var aBici = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" });
            Bicicleta.add(aBici, function (err, newBici) {

                if (err) console.log(err);
                Bicicleta.allBicis(function (err, bicis) {
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code)
                    done();
                });
            });

        });

    });

    describe("Bicicleta.findBycode", () => {

        it("debe devolver la bici con code", (done) => {
            Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" });
                Bicicleta.add(aBici, function (err, newBici) {

                    if (err) console.log(err);

                    var aBici2 = new Bicicleta({ code: 2, color: "rojo", modelo: "urbana" });
                    Bicicleta.add(aBici2, function (err, newBici) {
                        if (err) console.log(err);
                        Bicicleta.findByCode(1, function (error, targetBici) {
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);

                            done();
                        });

                    });

                });

            });

        })
    })

    describe("Bicicleta.removeByCode", () => {

        it("debe eliminar la bici con code", (done) => {
            Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" });
                Bicicleta.add(aBici, function (err, newBici) {

                    if (err) console.log(err);

                    var aBici2 = new Bicicleta({ code: 2, color: "rojo", modelo: "urbana" });
                    Bicicleta.add(aBici2, function (err, newBici) {
                        if (err) console.log(err);
                        Bicicleta.removeByCode(1, function (error, targetBici) {
                            if(error) console.log(error);

                            expect(aBici2.code).toBe(2);
                            
                            done();
                        });

                    });

                });

            });

        })
    })




});

//api basado en modelo estatico sin libreria de terceros como mongoDB
/*
beforeEach(() => { Bicicleta.allBicis = []; });

describe("bicicleta.allbicis", () => {

    it(("continua vacia"), () => {

        expect(Bicicleta.allBicis.length).toBe(0);


    });

});

describe("Bicicleta add", () => {

    it('agregamos una', () => {

        var a = new Bicicleta(1, 'rojo', 'urbana', [10.988781, -74.7915428]);
        Bicicleta.add(a);
        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);

    });
});

describe("Bicicletas.findById", () => {

    it("debe de volver la bici con id 1", () => {

        expect(Bicicleta.allBicis.length).toBe(0);

        var abici1 = new Bicicleta(1, "verde", "urbana");
        var abici2 = new Bicicleta(2, "blanco", "urbana");
        Bicicleta.add(abici1);
        Bicicleta.add(abici2);
        var targetBici = Bicicleta.findById(1);

        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(abici1.color);
        expect(targetBici.modelo).toBe(abici1.modelo);

    });
});

describe("Bicicleta.removeById", () => {

    it("debe devolver 1", () => {

        expect(Bicicleta.allBicis.length).toBe(0);



        var abici1 = new Bicicleta(1, "verde", "urbana");

        var abici2 = new Bicicleta(2, "verde", "urbana");
        Bicicleta.add(abici1);
        Bicicleta.add(abici2);

        Bicicleta.removeById(abici1.id);
        expect(Bicicleta.allBicis[0]).toBe(abici2);

    });

});*/
"use strict";

var nodemailer = require('nodemailer');

process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0; // Generate SMTP service account from ethereal.email

nodemailer.createTestAccount(function (err, account) {
  if (err) {
    console.error('Failed to create a testing account. ' + err.message);
    return process.exit(1);
  }

  console.log('Credentials obtained, sending message...'); // Create a SMTP transporter object

  var transporter = nodemailer.createTransport({
    host: 'smtp.ethereal.email',
    secure: false,
    port: 587,
    auth: {
      user: 'teresa.jakubowski@ethereal.email',
      pass: '8Uyqd9hSt5TgxB9UEJ'
    }
  }); // Message object

  var message = {
    from: 'teresa.jakubowski@ethereal.email',
    to: 'teresa.jakubowski@ethereal.email',
    subject: 'esto es una prueba ✔',
    text: 'hola comno estas',
    html: '<p><b>Hello</b> to myself!</p>'
  };
  transporter.sendMail(message, function (err, info) {
    if (err) {
      console.log('Error occurred. ' + err.message);
      return process.exit(1);
    }

    console.log('Message sent: %s', info.messageId); // Preview only available when sending through an Ethereal account

    console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
  });
});
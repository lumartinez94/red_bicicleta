var mymap = L.map('mapid').setView([10.9786428, -74.793333], 13);


L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoicHN5a29sYXgiLCJhIjoiY2tjcDNldWQ4MGh1NzJwb2JlZGZkZzlqbyJ9.cCeUkwGVVWlRNTgs80hhUA'
}).addTo(mymap);





var circle = L.circle([10.9786428, -74.793333], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 300,

}).addTo(mymap);






var marker = L.marker([10.9883666, -74.7926016]).addTo(mymap);
marker.bindPopup("Catedral Metropolitana María Reina de Barranquilla.<br>, Barranquilla, Atlántico").openPopup();


$.ajax({

    dataType: 'json',
    url: 'api/bicicletas',
    success: function (result) {

        console.log(result);
        result.bicicletas.forEach(function (bici) {
            L.marker(bici.ubicacion,{title:bici.id}).addTo(mymap);

        });
    }



});

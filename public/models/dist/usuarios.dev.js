"use strict";

var mongoose = require('mongoose');

var Reserva = require('./reserva');

var _require = require('express'),
    response = _require.response;

var uniqueValidator = require('mongoose-unique-validator');

var Schema = mongoose.Schema;

var bcrypt = require('bcrypt');

var saltRounds = 10;

var validateEmail = function validateEmail(email) {
  var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3,4})+$/;
  return re.test(email);
};

var usuarioSchema = new Schema({
  nombre: {
    type: String,
    trim: true,
    required: [true, 'El nombre es obligatorio']
  },
  email: {
    type: String,
    trim: true,
    required: [true, 'El email es obligatorio'],
    unique: true,
    lowercase: true,
    validate: [validateEmail, 'Por favor, ingrese un email valido'],
    match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3,4})+$/]
  },
  password: {
    type: String,
    required: [true, 'El password es obligatorio ']
  },
  passwordResetToken: String,
  passwordResetTokenExpires: Date,
  verificado: {
    type: Boolean,
    "default": false
  }
});
usuarioSchema.plugin(uniqueValidator, {
  message: 'El {PATH} ya existe con otro usuario'
});
usuarioSchema.pre('save', function (next) {
  if (this.isModified('password')) {
    this.password = bcrypt.hashSync(this.password, saltRounds);
  }

  next();
});

usuarioSchema.methods.validPassword = function (password) {
  return bcrypt.compareSync(password, this.password);
};

usuarioSchema.methods.reservar = function (biciId, desde, hasta, cb) {
  var reserva = new Reserva({
    usuario: this._id,
    bicicleta: biciId,
    desde: desde,
    hasta: hasta
  });
  console.log(reserva);
  reserva.save(cb);
};

module.exports = mongoose.model('Usuario', usuarioSchema);
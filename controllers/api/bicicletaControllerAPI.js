var Bicileta = require('../../public/models/bicicleta');
const Bicicleta = require('../../public/models/bicicleta');

exports.bicicleta_list = function (req, res) {

        Bicicleta.find({},function(err,bicicletas){
            res.status(200).json({
                bicicletas:bicicletas
            });

            
    
        });

    
}

exports.bicicleta_create = function (req, res) {

    var bici = new Bicicleta({code:req.body.code,color:req.body.color,modelo:req.body.modelo});
    bici.ubicacion = [req.body.lat, req.body.lng];

    bici.save(function(err){
        if(err) console.log(err);
        res.status(200).json(bici);
    });


}

exports.bicicleta_delete = function (req, res) {

    //Bicicleta.removeById(req.body.id);

    Bicicleta.deleteOne({id:req.body.id},function(error){
        
        res.status(204).send();

    });
    //no inteeresa la respuesta, no devolvera ningun json
    

}
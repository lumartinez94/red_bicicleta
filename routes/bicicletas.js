var express = require('express');
var router = express.Router();
var bicicletasController = require('../controllers/bicicleta');
const { route } = require('.');

router.get('/', bicicletasController.Bicicleta_list);
router.get('/create',bicicletasController.Bicicleta_create_Get);
router.post('/create',bicicletasController.Bicicleta_create_post);
router.post('/:id/delete',bicicletasController.Bicicleta_delete_post);
router.get('/:id/update',bicicletasController.Bicicleta_update_get);
router.post('/:id/update',bicicletasController.Bicicleta_update_post);

module.exports= router;